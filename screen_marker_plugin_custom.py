# Copy/link this to ~/pupil_capture_settings/plugins for a custom
# calibration choreography

from calibration_choreography.screen_marker_plugin import ScreenMarkerChoreographyPlugin



MARKER_SITES = [(0.1, .70), (.25, .70),  (.5, .70), (.75, .70), (0.9,.70),
                (0.1,.5), (.25, .5),(.5,.5), (.75,.5),(.9,.5),
                (0.1,.35), (.25, .35),(.5,.35), (.75,.35),(.9,.35),
                (0.1,.25), (.25, .25), (.5, .25), (.75, .25), (.9, .25),
                (0.2,0.15), (.8,0.15)]

class ScreenMarkerChoreographyCustomPlugin(ScreenMarkerChoreographyPlugin):
    label = "Screen marker custom calibration"
    
    @classmethod
    def selection_label(cls):
        return "Screen Custom"

    @staticmethod
    def get_list_of_markers_to_show(mode):
        return MARKER_SITES
